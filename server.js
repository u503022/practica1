var express = require('express');
var app     = express();
var bodyParser = require ('body-parser');
app.use(bodyParser.json()); //

var port    = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get ("/apitechu/v1",
  function(req, res) {
   console.log("GET /apitechu/v1");
   res.send({"msg" : "Hola desde APITechU"});
  }
);

app.get("/apitechu/v1/users",
  function(req, res) {
      console.log("GET /apitechu/v1/users");

      // res.sendFile('./usuarios.json'); // deprecated
      res.sendFile('usuarios.json', {root: __dirname});

      // var users = require('./usuarios.json');
      // res.send(users);
  }
);

app.post("/apitechu/v1/users",
  function(req, res){
    console.log("POST /apitechu/v1/users");
    /* se puede hacer el post en el header
    console.log(req.headers);
    console.log(req.headers.first_name);
    console.log(req.headers.last_name);
    console.log(req.headers.country);

    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" : req.headers.country
    };
    */

    // o se puede hacer el post en el body
    console.log("first_name is " + req.body.firsname);
    console.log("last_name is " + req.body.last_name);
    console.log("country is " + req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    // metiendo usuarios en una variable
    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);
    var msg = "Usuario guardado con éxito";
    console.log(msg);
    res.send({"msg" : msg});
  }
)

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);

    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg" : "Usuario borrado"});
  }
)

// función de escritura de datos en el fichero
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if(err){
        console.log(err);
      } else{
        console.log("Datos escritos en archivo");
      }
    }
  )
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
)
